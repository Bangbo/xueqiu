import sys
sys.path.append('..')

import pytest

from Pages.app import App


class TestMain:

    def setup(self):
        self.main = App().start().main()

    def teardown(self):
        pass

    # @allure.suite("搜索一个股票，能够搜索到正确的内容")
    @pytest.mark.parametrize(
        ["search_key", "expect"],
        [
            ("alibaba", "阿里巴巴"),
            ("jd", "京东"),
            ("腾讯", "腾讯控股")
        ])
    def test_search(self, search_key, expect):
        assert expect in self.main.goto_search_page().search(search_key).get_name_first_item()

    # @allure.suite("能从主页进入消息页")
    def test_goto_message(self):
        assert "消息" in self.main.goto_message_page().get_title()

# # 进行旧测试数据的清理，测试报告的生成和展示
# if __name__ == "__main__":
#
#     # 清空allure_results文件夹，清理掉allure历史记录
#     for i in os.listdir(r'allure_results'):
#         os.remove('allure_results/{}'.format(i))
#     time.sleep(1)
#
#     # 执行测试并保存allure需要的结果
#     os.system('pytest -v --alluredir=allure_results {}'.format(__file__))
#     time.sleep(1)
#
#     # 使用allure展示测试报告
#     os.system(r'allure serve allure_results')
