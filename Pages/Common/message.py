from selenium.webdriver.common.by import By

from Pages.base_page import BasePage


class Message(BasePage):
    def get_title(self):
        return self.find(By.ID, "action_title").text