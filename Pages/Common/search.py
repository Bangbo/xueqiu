from selenium.webdriver.common.by import By

from Pages.base_page import BasePage


class Search(BasePage):
    def search(self, key: str):
        self.find(By.ID, 'search_input_text').send_keys(key)
        # 点击只能推荐的第一项
        self.find(By.XPATH, '//*[@resource-id="com.xueqiu.android:id/listview"]/android.widget.RelativeLayout[1]').click()
        return self

    def get_name_first_item(self):
        return self.find(By.ID, 'stockName').text

    def get_price_first_item(self):
        return self.find(By.ID, "current_price").text
