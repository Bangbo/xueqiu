from selenium.webdriver.common.by import By

from Pages.base_page import BasePage
from Pages.Common.message import Message
from Pages.Common.search import Search


class Main(BasePage):
    def goto_search_page(self):
        self.find(By.ID, "tv_search").click()
        return Search(self._driver)

    def goto_message_page(self):
        self.find(By.ID, "ll_icon").click()
        return Message(self._driver)