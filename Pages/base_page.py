from appium.webdriver.webdriver import WebDriver
from selenium.webdriver.common.by import By


class BasePage:
    _driver: WebDriver
    _black_list = [
        (By.ID, 'tv_agree')
    ]
    _error_count = 0
    _error_max = 3

    def __init__(self, driver: WebDriver = None):
        self._driver = driver

    def find(self, locator, value: str = None):
        try:
            element = self._driver.find_element(*locator) if isinstance(locator, tuple) else self._driver.find_element(
                locator, value)
            self._error_count = 0
            return element
        except Exception as e:
            self._error_count += 1
            # 跳出循环
            if self._error_count >= self._error_max:
                raise e

            for bl_element in self._black_list:
                bl_elements = self._driver.find_elements(*bl_element)
                if len(bl_elements) > 0:
                    bl_elements[0].click()
                    # 递归
                    return self.find(locator, value)

            raise e
